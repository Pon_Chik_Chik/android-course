package ru.intersvyaz.android_course.ui.main

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import okhttp3.Protocol
import okhttp3.Response
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi
import ru.intersvyaz.android_course.di.useCases.GetUserUseCase
import ru.intersvyaz.android_course.di.useCases.GetUsersUseCase
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase,
    private val getUserUseCase: GetUserUseCase,
) : ViewModel() {

    private val _users = MutableStateFlow<List<User>>(listOf())
    val users = _users.asStateFlow()

    private val _user = MutableStateFlow<User?>(null)
    val user = _user.asStateFlow()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.d("Exception", throwable.toString())
    }

    fun getUsers() {
        viewModelScope.launch(exceptionHandler) {
            getUsersUseCase.invoke()
                .map { users ->
                    users.map { it.toUser() }
                }
                .filterNotNull()
                .collect {
                    _users.emit(it)
                }
        }
    }

    fun getUser(userId: Int) {
        viewModelScope.launch(exceptionHandler) {
            getUserUseCase(userId).collect {
                _user.emit(it)
            }
        }
    }
}