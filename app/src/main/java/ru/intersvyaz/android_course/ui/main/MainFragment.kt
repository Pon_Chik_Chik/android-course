package ru.intersvyaz.android_course.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.intersvyaz.android_course.utils.Consts.ACTION_START_OR_RESUME_SERVICE
import ru.intersvyaz.android_course.R
import ru.intersvyaz.android_course.data.model.Icon
import ru.intersvyaz.android_course.data.model.Memory
import ru.intersvyaz.android_course.databinding.FragmentMainBinding
import ru.intersvyaz.android_course.services.MyService
import ru.intersvyaz.android_course.ui.adapters.CardAdapter
import ru.intersvyaz.android_course.ui.adapters.IconAdapter
import ru.intersvyaz.android_course.workers.MyWorker
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private val binding: FragmentMainBinding by viewBinding()
    private val viewModel: MainViewModel by viewModels()

    private val cardAdapter: CardAdapter by lazy { CardAdapter() }
    private val iconAdapter: IconAdapter by lazy { IconAdapter() }

    private val navigateListener = View.OnClickListener {
        MainFragmentDirections.navToSecondFragment().apply {
            findNavController().navigate(this)
        }
    }

    private val cardImageCLickListener = CardAdapter.CardImageClickListener {
        simpleWork()
    }

    private val cardTitleCLickListener = CardAdapter.CardTitleClickListener {
        sendCommandToService(ACTION_START_OR_RESUME_SERVICE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getUsers()
        viewModel.getUser(1)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardAdapter.cardImageClickListener = cardImageCLickListener
        cardAdapter.cardTitleClickListener = cardTitleCLickListener
        with(binding) {
            cardList.adapter = cardAdapter
            cardList.layoutManager = LinearLayoutManager(requireContext())
            iconList.adapter = iconAdapter
        }

        iconAdapter.setList(iconList)

        getUsers()
        getUser()
    }

    private fun getUser() {
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.user.collect {
                    binding.createText.text = it?.name
                }
            }
        }
    }

    private fun getUsers() {
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.users.collect { cardAdapter.setList(it) }
            }
        }
    }

    private fun sendCommandToService(action: String) =
        Intent(requireContext(), MyService::class.java).also {
            it.action = action
            requireContext().startService(it)
        }

    private fun simpleWork() {
        val constraint: Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .build()

        val request: PeriodicWorkRequest =
            PeriodicWorkRequestBuilder<MyWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraint)
                .setBackoffCriteria(
                    BackoffPolicy.LINEAR,
                    PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                    TimeUnit.MILLISECONDS
                )
                .build()

        WorkManager.getInstance(requireContext())
            .enqueueUniquePeriodicWork("work", ExistingPeriodicWorkPolicy.KEEP, request)
    }

    companion object {
        fun newInstance() = MainFragment()

        private val iconList = listOf(
            Icon.Active(0, "zero"),
            Icon.Disable(1, "first"),
            Icon.Active(2, "second"),
            Icon.Disable(3, "third"),
            Icon.Active(2, "second"),
            Icon.Disable(3, "third"),
            Icon.Active(2, "second"),
            Icon.Disable(3, "third"),
            Icon.Active(2, "second"),
        )

        private val list = listOf(
            Memory(0, "zero", "zero desc", ""),
            Memory(1, "first", "first desc", ""),
            Memory(2, "second", "second desc", ""),
            Memory(3, "third", "third desc", ""),
            Memory(4, "forth", "forth desc", "")
        )
    }
}