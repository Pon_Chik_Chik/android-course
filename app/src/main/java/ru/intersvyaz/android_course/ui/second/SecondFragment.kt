package ru.intersvyaz.android_course.ui.second

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.intersvyaz.android_course.utils.Consts.ACTION_STOP_SERVICE
import ru.intersvyaz.android_course.R
import ru.intersvyaz.android_course.databinding.FragmentSecondBinding
import ru.intersvyaz.android_course.services.MyService

class SecondFragment : Fragment(R.layout.fragment_second) {

    private val binding: FragmentSecondBinding by viewBinding()

    private val navigateListener = View.OnClickListener {
        SecondFragmentDirections.navToMainFragment().apply {
            findNavController().navigate(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            navigationButtonSecond.setOnClickListener(navigateListener)
            stopServiceButton.setOnClickListener {
                Intent(requireContext(), MyService::class.java).also {
                    it.action = ACTION_STOP_SERVICE
                    requireContext().startService(it)
                }
            }
        }

    }

    companion object {
        fun newInstance() = SecondFragment()
    }
}