package ru.intersvyaz.android_course.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.intersvyaz.android_course.data.model.Memory
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.databinding.CardViewHolderBinding

class CardAdapter : RecyclerView.Adapter<CardAdapter.CardViewHolder>() {

    private val items: MutableList<User> = mutableListOf()

    var cardImageClickListener: CardImageClickListener? = null
    var cardTitleClickListener: CardTitleClickListener? = null

    fun setList(list: List<User>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CardViewHolderBinding.inflate(inflater, parent, false)

        return CardViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: CardViewHolder,
        position: Int
    ) {
        val item = items[position]
        holder.onBind(item)
    }

    override fun getItemCount(): Int = items.size

    inner class CardViewHolder(private val binding: CardViewHolderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(item: User) {
            with(binding) {
                title.text = item.name
                subTitle.text = item.email
                Glide.with(image.context).load(item.avatar).into(image)
                image.setOnClickListener { cardImageClickListener?.onClick() }
                title.setOnClickListener { cardTitleClickListener?.onClick() }
            }
        }
    }

    fun interface CardImageClickListener {
        fun onClick()
    }

    fun interface CardTitleClickListener {
        fun onClick()
    }
}