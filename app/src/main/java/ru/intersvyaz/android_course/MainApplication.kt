package ru.intersvyaz.android_course

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import dagger.hilt.android.HiltAndroidApp
import ru.intersvyaz.android_course.utils.Consts.SERVICE_CHANNEL_ID
import ru.intersvyaz.android_course.utils.Consts.WORKER_CHANNEL_ID

@HiltAndroidApp
class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        val workerChannel = NotificationChannel(
            WORKER_CHANNEL_ID,
            "Worker Channel",
            NotificationManager.IMPORTANCE_HIGH
        )
        val serviceChannel = NotificationChannel(
            SERVICE_CHANNEL_ID,
            "SERVICE Channel",
            NotificationManager.IMPORTANCE_HIGH
        )
        val notificationManager = getSystemService(NotificationManager::class.java)
        with(notificationManager) {
            createNotificationChannel(workerChannel)
            createNotificationChannel(serviceChannel)
        }
    }
}