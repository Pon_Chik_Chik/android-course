package ru.intersvyaz.android_course.utils

enum class RequestStatus {
    SUCCESS,
    ERROR,
    LOADING
}