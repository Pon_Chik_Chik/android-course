package ru.intersvyaz.android_course.services

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import ru.intersvyaz.android_course.utils.Consts
import ru.intersvyaz.android_course.utils.Consts.ACTION_OPEN_SECOND_FRAGMENT
import ru.intersvyaz.android_course.utils.Consts.ACTION_PAUSE_SERVICE
import ru.intersvyaz.android_course.utils.Consts.ACTION_START_OR_RESUME_SERVICE
import ru.intersvyaz.android_course.utils.Consts.ACTION_STOP_SERVICE
import ru.intersvyaz.android_course.utils.Consts.SERVICE_NOTIFICATION_ID
import ru.intersvyaz.android_course.MainActivity
import ru.intersvyaz.android_course.R

class MyService : LifecycleService() {
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (it.action) {
                ACTION_START_OR_RESUME_SERVICE -> {
                    startForegroundService()
                    Log.e("MyService", "Start or resume")
                }
                ACTION_PAUSE_SERVICE -> {

                    Log.e("MyService", "Pause")
                }
                ACTION_STOP_SERVICE -> {
                    stopForeground(true)
                    Log.e("MyService", "Stop")
                }
                else -> {}
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun getMainActivityPendingIntent() =
        PendingIntent.getActivity(
            this,
            0,
            Intent(this, MainActivity::class.java).also {
                it.action = ACTION_OPEN_SECOND_FRAGMENT
            },
            FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE
        )

    private fun startForegroundService() {
        NotificationCompat.Builder(this, Consts.SERVICE_CHANNEL_ID)
            .setSmallIcon(R.drawable.icon_background)
            .setContentText("Service work")
            .setContentTitle("My first foreground service")
            .setContentIntent(getMainActivityPendingIntent())
            .build()
            .also {
                startForeground(SERVICE_NOTIFICATION_ID, it)
            }
    }
}