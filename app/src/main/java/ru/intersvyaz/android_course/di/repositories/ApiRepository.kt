package ru.intersvyaz.android_course.di.repositories

import kotlinx.coroutines.flow.Flow
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi

interface ApiRepository {

    fun getUsers(): Flow<List<UserApi>>

    fun getUser(userId: Int): Flow<User>
}