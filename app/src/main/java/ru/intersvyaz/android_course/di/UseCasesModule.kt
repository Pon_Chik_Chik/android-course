package ru.intersvyaz.android_course.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import ru.intersvyaz.android_course.di.useCases.GetUserUseCase
import ru.intersvyaz.android_course.di.useCases.GetUsersUseCase
import ru.intersvyaz.android_course.di.useCases.impl.GetUserUseCaseImpl
import ru.intersvyaz.android_course.di.useCases.impl.GetUsersUseCaseImpl

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCasesModule {

    @Binds
    abstract fun bindsGetUsersUseCase(getUsersUseCaseImpl: GetUsersUseCaseImpl): GetUsersUseCase

    @Binds
    abstract fun bindsGetUserUseCase(getUserUseCaseImpl: GetUserUseCaseImpl): GetUserUseCase
}