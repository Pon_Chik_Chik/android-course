package ru.intersvyaz.android_course.di.useCases.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.di.repositories.ApiRepository
import ru.intersvyaz.android_course.di.useCases.GetUserUseCase
import javax.inject.Inject

class GetUserUseCaseImpl @Inject constructor(
    private val apiRepository: ApiRepository,
) : GetUserUseCase {

    override fun invoke(userId: Int): Flow<User> = apiRepository
        .getUser(userId)
        .flowOn(Dispatchers.IO)
}