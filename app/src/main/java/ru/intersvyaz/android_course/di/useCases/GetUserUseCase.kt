package ru.intersvyaz.android_course.di.useCases

import kotlinx.coroutines.flow.Flow
import ru.intersvyaz.android_course.data.model.User

interface GetUserUseCase {

    operator fun invoke(userId: Int): Flow<User>
}