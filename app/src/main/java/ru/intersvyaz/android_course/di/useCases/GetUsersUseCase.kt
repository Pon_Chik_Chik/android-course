package ru.intersvyaz.android_course.di.useCases

import kotlinx.coroutines.flow.Flow
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi

interface GetUsersUseCase {

    operator fun invoke(): Flow<List<UserApi>>
}