package ru.intersvyaz.android_course.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.intersvyaz.android_course.di.repositories.ApiRepository
import ru.intersvyaz.android_course.di.repositories.impl.ApiRepositoryImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindsApiRepository(apiRepositoryImpl: ApiRepositoryImpl): ApiRepository
}