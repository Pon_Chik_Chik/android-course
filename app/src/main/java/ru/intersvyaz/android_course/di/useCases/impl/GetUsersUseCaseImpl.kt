package ru.intersvyaz.android_course.di.useCases.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi
import ru.intersvyaz.android_course.di.repositories.ApiRepository
import ru.intersvyaz.android_course.di.useCases.GetUsersUseCase
import javax.inject.Inject

class GetUsersUseCaseImpl @Inject constructor(
    private val apiRepository: ApiRepository,
) : GetUsersUseCase {

    override fun invoke(): Flow<List<UserApi>> = apiRepository.getUsers().flowOn(Dispatchers.IO)
}