package ru.intersvyaz.android_course.di.repositories.impl

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import ru.intersvyaz.android_course.data.api.ApiService
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi
import ru.intersvyaz.android_course.di.repositories.ApiRepository
import javax.inject.Inject

class ApiRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : ApiRepository {

    override fun getUsers(): Flow<List<UserApi>> = flow {
        val api = apiService.getUsers()
        emit(api)
    }

    override fun getUser(userId: Int): Flow<User> = flow {
        val api = apiService.getUser(userId).toUser()
        emit(api)
    }
}