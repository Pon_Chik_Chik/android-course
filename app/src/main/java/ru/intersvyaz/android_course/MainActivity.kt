package ru.intersvyaz.android_course

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.intersvyaz.android_course.utils.Consts.ACTION_OPEN_SECOND_FRAGMENT

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val navController by lazy {
        val navFragment =
            supportFragmentManager.findFragmentById(R.id.navContainer) as NavHostFragment
        navFragment.findNavController()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigateToSecondFragment(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        navigateToSecondFragment(intent)
    }

    private fun navigateToSecondFragment(intent: Intent?) {
        if (intent?.action == ACTION_OPEN_SECOND_FRAGMENT) {
            navController.navigate(R.id.openSecondFragment)
        }
    }
}