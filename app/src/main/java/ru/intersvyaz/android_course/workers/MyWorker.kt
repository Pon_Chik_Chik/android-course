package ru.intersvyaz.android_course.workers

import android.content.Context
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkerParameters
import kotlinx.coroutines.delay
import ru.intersvyaz.android_course.utils.Consts.WORKER_CHANNEL_ID
import ru.intersvyaz.android_course.R
import kotlin.random.Random

class MyWorker(
    private val context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result {
        return try {
            showNotification()
            delay(500L)
            Log.e("MyWorker", "Success work")
            Result.success()
        } catch (exception: Exception) {
            Log.e("MyWorker", "Error on do work")
            Result.failure()
        }
    }

    private suspend fun showNotification() {
        setForeground(
            ForegroundInfo(
                Random.nextInt(),
                NotificationCompat.Builder(context, WORKER_CHANNEL_ID)
                    .setSmallIcon(R.drawable.icon_background)
                    .setContentText("Worker work")
                    .setContentTitle("My first worker")
                    .build()
            )
        )
    }

}