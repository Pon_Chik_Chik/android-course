package ru.intersvyaz.android_course.data.model

import com.google.gson.annotations.SerializedName

data class UserApi(
    @SerializedName("avatar")
    val image: String,
    @SerializedName("email")
    val userEmail: String,
    @SerializedName("id")
    val userId: String,
    @SerializedName("name")
    val userName: String,
) {
    fun toUser() = User(
        image,
        userEmail,
        userId,
        userName,
    )
}
