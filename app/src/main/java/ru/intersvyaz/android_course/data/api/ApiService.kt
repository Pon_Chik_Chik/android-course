package ru.intersvyaz.android_course.data.api

import retrofit2.http.GET
import retrofit2.http.HEAD
import retrofit2.http.Path
import ru.intersvyaz.android_course.data.model.User
import ru.intersvyaz.android_course.data.model.UserApi

interface ApiService {

    @GET("users")
    suspend fun getUsers(): List<UserApi>

    @GET("users/{id}")
    suspend fun getUser(@Path("id") userId: Int): UserApi
}