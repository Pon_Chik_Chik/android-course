package ru.intersvyaz.android_course.data.model

data class Memory(
    val id: Int,
    val title: String,
    val description: String,
    val image: String,
)
