package ru.intersvyaz.android_course.data.model

data class User(
    val avatar: String,
    val email: String,
    val id: String,
    val name: String,
) {
    fun toUserApi() = UserApi(
        image = avatar,
        userEmail = email,
        userId = id,
        userName = name
    )
}
